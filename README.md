# MySql

O banco de dados está no arquivo demo.sql

--

# Back ( node versão atual )

1 - No arquivo dbconnection.js

host : 'SEU_HOST'

user : 'SEU_USUARIO_BANCO'

port : 'PORTA_DO_BANCO'

password: 'PASSWORD_DO_BANCO'

database: 'NOME_DO_BANCO_MYSQL'

2 - Para rodar a api, digite "npm start"

--

# Front ( Angular 6+ )

1 - Para rodar o front rode o comando "npm i"

2 - Para iniciar o servidor "ng serve --open

3 - A url das requisições é http://0.0.0.0:3000, caso mude por favor trocar nos components

4 - Estou enviando com o node_modules já instalado, se der problema é só remover e instalar com "npm i"

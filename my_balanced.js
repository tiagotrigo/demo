/*
  1 - Remova caracteres irrelevantes.
  2 - Substitua qualquer instância de () ou [] ou {} com a cadeia vazia
  3 - Caso contrário, volte ao passo 2.
*/

function MyBalanced(str) {

  let cur = removeCruft(str), next;

  while (next = removeMatched(cur)) {
      if (next === cur) return false;
      cur = next;
  }

  return true;

  function removeMatched(str) { 
    return str.replace(/\(\)|\[\]|{}/g,''); 
  }

  function removeCruft(str) { 
    return str.replace(/[^(){}[\]]/g,'');
  }

}
// Exemplos
MyBalanced('(){}[]') 
MyBalanced('[{()}](){}')
MyBalanced('[]{()')
MyBalanced('[{)]')
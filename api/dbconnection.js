'use strict';

const mysql = require('mysql');

const connection = mysql.createPool({
	host: 'localhost',
	user: 'root',
	port: 8889,
	password: 'root',
	database: 'demo'
});

module.exports = connection;
const express = require('express');
const router = express.Router();
// Routes
router.get('/', (req, res, next) => {
	res.render('index', { title: 'API' })
});

module.exports = router;
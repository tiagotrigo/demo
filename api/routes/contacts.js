const express = require('express');
const router = express.Router();
const Contact = require('../models/Contact');

// Select
router.get('/:id?', (req, res, next) => {
	if (req.params.id) {
		Contact.getById(req.params.id, (err, rows) => {
			if (err) {
				res.json(err);
			} else {
				res.json(rows);
			}
		})
	} else {
		Contact.getAll((err, rows) => {
			if (err) {
				res.json(err);
			} else {
				res.json(rows);
			}
		});
	}
});
// Add
router.post('/', (req, res, next) => {  
    Contact.add(req.body, (err, count) => {  
        if (err) {  
            res.json(false);  
        } else {  
            res.json(true);
        }  
    });  
});   
// Edit 
router.put('/:id', (req, res, next) => {  
    Contact.update(req.params.id, req.body, (err, rows) => {  
        if (err) {  
            res.json(false);  
        } else {  
            res.json(true);  
        }  
    });  
});
// Delete
router.delete('/:id', (req, res, next) => {  
    Contact.delete(req.params.id, (err, count) => {  
        if (err) {  
            res.json(err);  
        } else {  
            res.json(count);  
        }  
    });  
});

module.exports = router;
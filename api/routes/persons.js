const express = require('express');
const router = express.Router();
const Person = require('../models/Person');

// Select
router.get('/:id?', (req, res, next) => {
	if (req.params.id) {
		Person.getById(req.params.id, (err, rows) => {
			if (err) {
				res.json(err);
			} else {
				res.json(rows);
			}
		})
	} else {
		Person.getAll((err, rows) => {
			if (err) {
				res.json(err);
			} else {
				res.json(rows);
			}
		});
	}
});
// Add
router.post('/', (req, res, next) => {  
    Person.add(req.body, (err, count) => {  
        if (err) {  
            res.json(false);  
        } else {  
            res.json(true); // 0 ou 1
        }  
    });  
});   
// Edit 
router.put('/:id', (req, res, next) => {  
    Person.update(req.params.id, req.body, (err, rows) => {  
        if (err) {  
            res.json(false);  
        } else {  
            res.json(true);  
        }  
    });  
});
// Delete
router.delete('/:id', (req, res, next) => {  
    Person.delete(req.params.id, (err, count) => {  
        if (err) {  
            res.json(err);  
        } else {  
            res.json(req.body); // 0 ou 1 
        }  
    });  
});

module.exports = router;
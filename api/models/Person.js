const db = require('../dbconnection'); 

const Person = {  
    getAll: (callback) => {
        return db.query("SELECT * FROM person ORDER BY id DESC", callback);  
    },  
    getById: (id, callback) => {  
        return db.query("SELECT * FROM person WHERE id = ?", [id], callback);  
    },  
    add: (Person, callback) => {  
        return db.query("INSERT INTO person VALUES (?, ?)", [Person.id, Person.name], callback);  
    },  
    delete: (id, callback) => {  
        return db.query("DELETE FROM person WHERE id=?", [id], callback);  
    },  
    update: (id, Person, callback) => {  
        return db.query("UPDATE person SET name = ? WHERE id = ?", [Person.name, id], callback);  
    }  
};  

module.exports = Person;
const db = require('../dbconnection'); 

const Contact = {  
    getAll: (callback) => {
        return db.query("SELECT * FROM contact ORDER BY id DESC", callback);  
    },  
    getById: (id, callback) => {  
        return db.query("SELECT * FROM contact WHERE id = ?", [id], callback);  
    },  
    add: (Contact, callback) => {  
        return db.query("INSERT INTO contact VALUES (?, ?, ?, ?, ?)", [Contact.id, Contact.id_person, Contact.phone, Contact.email, Contact.whatsapp], callback);  
    },  
    delete: (id, callback) => {  
        return db.query("DELETE FROM contact WHERE id=?", [id], callback);  
    },  
    update: (id, Contact, callback) => {  
        return db.query("UPDATE contact SET id_person = ?, phone = ?, email = ?, whatsapp = ? WHERE id = ?", [Contact.id_person, Contact.phone, Contact.email, Contact.whatsapp, id], callback);  
    }  
};  

module.exports = Contact;